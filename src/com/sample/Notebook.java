package com.sample;

public class Notebook extends Book {

	public void draw() {
		System.out.println("draw a pic");
	}

	@Override
	public void write() {
		System.out.println("write a book");
	}

	@Override
	public void read() {
         System.out.println("read a book");		
	}
	
	public static void main(String[] args) {
		
		Notebook n= new Notebook();
		n.draw();
		n.write();
		n.read();
		
	}
	
	
	
	
	
	
	
}
