package com.sample;

public class JavaClass01 {

	int a=0;  //instance variable
	static int b=5; // static variable
	public void sample() {
		int c=20; //local variable
		a=a+5;
		System.out.println(a);
		
	}
	
	public static void main(String[] args) {
		JavaClass01 jc= new JavaClass01();
		JavaClass01 jc1= new JavaClass01();
		a=a+20;  //we can't call a instance variable without creating an object
		System.out.println(a);
		b=b-5; //we can access static variable without using an object
		System.out.println(b);
		c=c*2; // The scope of local variable is only in the method
	}
	
	
}
